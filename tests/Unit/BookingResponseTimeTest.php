<?php

namespace Tests\Unit;

use Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Tests\TestCase;

class BookingResponseTimeTest extends TestCase
{

    /**
     * @dataProvider successDateProvider
     *
     * @param $bookingDate
     * @param $responseDate
     * @param $officeHours
     * @param $expectedMinutes
     */
    public function testSuccess($bookingDate, $responseDate, $officeHours, $expectedMinutes)
    {
        $calculator = new ResponseTimeCalculator();

        $minutes = $calculator->calculate($bookingDate, $responseDate, $officeHours);

        $this->assertEquals($expectedMinutes, $minutes);
    }


    public function testFailureBookingDate()
    {
        $calculator = new ResponseTimeCalculator();

        $this->expectException(\InvalidArgumentException::class);

        $calculator->calculate('2018-01-01 17:00', '2018-01-01 09:00', []);
    }


    public function testFailureOfficeHours()
    {
        $calculator = new ResponseTimeCalculator();

        $this->expectException(\InvalidArgumentException::class);

        $calculator->calculate('2018-01-01 09:00', '2018-12-12 17:00', []);
    }


    public function testFailureIsClosed()
    {
        $calculator = new ResponseTimeCalculator();

        $this->expectException(\InvalidArgumentException::class);

        $calculator->calculate('2018-01-01 09:00', '2018-12-12 17:00', [
            1 => ['from' => '09:00', 'to' => '17:00'],
        ]);
    }


    /**
     * @return \Generator
     */
    public function successDateProvider()
    {
        yield [
            '2018-01-01 00:00',
            '2018-01-01 09:30',
            [
                1 => $this->officeDayFactory('09:00', '17:00'),
            ],
            30,
        ];

        yield [
            '2018-01-01 09:05',
            '2018-01-01 09:30',
            [
                1 => $this->officeDayFactory('09:00', '17:00'),
            ],
            25,
        ];

        yield [
            '2018-01-01 16:30',
            '2018-01-02 09:30',
            [
                1 => $this->officeDayFactory('09:00', '17:00'),
                $this->officeDayFactory('09:00', '17:00'),
            ],
            60,
        ];

        yield [
            '2018-01-01 16:30',
            '2018-01-03 09:30',
            [
                1 => $this->officeDayFactory('09:00', '17:00'),
                $this->officeDayFactory('09:00', '17:00', true),
                $this->officeDayFactory('09:00', '17:00'),
            ],
            60,
        ];

        yield [
            '2018-01-01 16:30',
            '2018-01-05 09:30',
            [
                1 => $this->officeDayFactory('09:00', '17:00'),
                $this->officeDayFactory('09:00', '17:00', true),
                $this->officeDayFactory('09:00', '17:00'),
                $this->officeDayFactory('09:00', '17:00', true),
                $this->officeDayFactory('09:00', '17:00'),
            ],
            540,
        ];
    }


    /**
     * @param $from
     * @param $to
     * @param bool $isClosed
     * @return array
     */
    private function officeDayFactory($from, $to, $isClosed = false)
    {
        return [
            'from'     => $from,
            'to'       => $to,
            'isClosed' => $isClosed,
        ];
    }
}
