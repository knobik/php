<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 *
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


use Carbon\Carbon;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{

    /**
     * @param $bookingDateTime
     * @param $responseDateTime
     * @param $officeHours
     * @return int
     */
    public function calculate($bookingDateTime, $responseDateTime, $officeHours): int
    {

        if (!$bookingDateTime instanceof Carbon) {
            $bookingDateTime = new Carbon($bookingDateTime);
        }

        if (!$responseDateTime instanceof Carbon) {
            $responseDateTime = new Carbon($responseDateTime);
        }

        if ($responseDateTime < $bookingDateTime) {
            throw new \InvalidArgumentException('Response date cant be smaller then booking date');
        }

        $minutes = 0;

        $baseDate = $bookingDateTime->copy();
        while (true) {
            if ($baseDate >= $responseDateTime) {
                break;
            }

            $officeDay = $this->officeDay($baseDate->dayOfWeek, $officeHours);

            if (!$this->isClosed($officeDay)) {

                $dateFrom = $this->officeHourBaseDate($baseDate, $officeDay, 'from');
                $dateTo = $this->officeHourBaseDate($baseDate, $officeDay, 'to');

                if ($dateFrom < $baseDate) {
                    $dateFrom = $baseDate->copy();
                }

                while (true) {
                    if ($dateFrom >= $dateTo || $dateFrom >= $responseDateTime) {
                        break;
                    }

                    $minutes++;
                    $dateFrom->addMinute();
                }
            }

            $baseDate = new Carbon($baseDate->addDay()->format('Y-m-d 00:00'));
        }

        return $minutes;
    }


    /**
     * @param array $officeDay
     * @return bool
     */
    private function isClosed(array $officeDay): bool
    {
        if (!isset($officeDay['isClosed'])) {
            throw new \InvalidArgumentException('Invalid data supplied.');
        }

        return (bool)$officeDay['isClosed'];
    }


    /**
     * @param Carbon $baseDate
     * @param array $officeDay
     * @param string $type
     * @return Carbon
     */
    private function officeHourBaseDate(Carbon $baseDate, array $officeDay, string $type): Carbon
    {
        if (!isset($officeDay[$type])) {
            throw new \InvalidArgumentException('Invalid type supplied.');
        }

        return new Carbon($baseDate->format('Y-m-d').' '.$officeDay[$type]);
    }


    /**
     * @param $officeHours
     * @param $dayOfWeek
     * @return array
     */
    private function officeDay($dayOfWeek, $officeHours): array
    {
        if (!isset($officeHours[$dayOfWeek])) {
            throw new \InvalidArgumentException('Day of week missing in office hours.');
        }

        return $officeHours[$dayOfWeek];
    }
}
