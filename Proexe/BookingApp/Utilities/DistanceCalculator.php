<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 *
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator
{

    const EARTH_RADIUS = 6371000;


    /**
     * @param array $from
     * @param array $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate(array $from, array $to, string $unit = 'm')
    {
        if (!in_array($unit, ['m', 'km'])) {
            throw new \InvalidArgumentException('Invalid SI unit. Only meters and kilometers allowed! (m|km)');
        }

        list($latFrom, $lngFrom) = $from;
        list($latTo, $lngTo) = $to;

        // math magic ;) thx stackoverflow.

        $latFrom = deg2rad($latFrom);
        $lngFrom = deg2rad($lngFrom);
        $latTo = deg2rad($latTo);
        $lngTo = deg2rad($lngTo);

        $lonDelta = $lngTo - $lngFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        $distance = $angle * static::EARTH_RADIUS;

        return $this->calculateUnits($distance, $unit);
    }


    /**
     * @param array $from
     * @param array $offices
     *
     * @return array
     */
    public function findClosestOffice($from, array $offices)
    {

        /** MySQL
            set @lat=14.12232322;
            set @lng=8.12232322;

            SELECT id, name, lat, lng, 111.045 * DEGREES(ACOS(COS(RADIANS(@lat))
             * COS(RADIANS(lat))
             * COS(RADIANS(lng) - RADIANS(@lng))
             + SIN(RADIANS(@lat))
             * SIN(RADIANS(lat))))
             AS distance_in_km
            FROM offices
            ORDER BY distance_in_km ASC
            LIMIT 1;
         */

        $offices = collect($offices)
            ->map(function ($office) use ($from) {
                $office['distance'] = $this->calculate($from, [$office['lat'], $office['lng']]);

                return $office;
            })
            ->sortBy(function ($office) {
                return $office['distance'];
            });

        $closest = $offices->first();

        return $closest['name'];
    }


    /**
     * @param float $distance
     * @param string $unit
     * @return float
     */
    private function calculateUnits($distance, string $unit): float
    {
        switch ($unit) {

            case 'km':
                $distance = $distance / 1000;
                break;

            case 'm':
            default:
                break;
        }

        return $distance;
    }

}
